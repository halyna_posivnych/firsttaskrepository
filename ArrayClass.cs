using System;

namespace OneDimensionalArrayNonZeroBound
{
	// This class represents one dimensional array with non zero bound.
	class OneDArrayNonZeroBound : IComparable<OneDArrayNonZeroBound>
	{
		private int _length;
		private int _lowerBound;
		private int[] _array;
		
		#region Constructors
		
		// Creates array of int with zero bound and number of elements specified in arguments.
		public OneDArrayNonZeroBound(int length)
		{
			if (length < 0)
			{
				throw new ArgumentException("Length must be > 0 !!!", "length");
			}
			_length = length;
			_lowerBound = 0;
			_array = new int[length];
		}
		
		// Creates array of int with zero bound and fill array with elements specified in arguments.
		public OneDArrayNonZeroBound(int[] array)
		{
			_length = array.Length;
			_lowerBound = 0;
			for (int i = 0; i < array.Length; i++)
			{
				_array[i] = array[i];
			}
		}
		
		// Copy constructor.
		public OneDArrayNonZeroBound(OneDArrayNonZeroBound array)
		{
			_length = array._length;
			_lowerBound = array._lowerBound;
			_array = new int[_length];
			for (int i = 0; i < _length; i++)
			{
				_array[i] = array[i + array._lowerBound];
			}
		}
		
		// Creates array of int with specified lower bound and number of elements.
		public OneDArrayNonZeroBound(int length, int lowerBound)
		{
			if (length < 0)
			{
				throw new ArgumentException("Length must be > 0 !!!", "length");
			}
			_length = length;
			_lowerBound = lowerBound;
			_array = new int[_length];
		}
		#endregion
		
		#region Properties
		
		// Gets length of array.
		public int Length
		{
			get
			{
				return _length;
			}
		}
		
		// Gets lower bound of array.
		public int LowerBound
		{
			get
			{
				return _lowerBound;
			}
		}
		
		// Gets upper bound of array.
		public int UpperBound
		{
			get
			{
				return _lowerBound + _length;
			}
		}
		
		// Gets and sets specified element in array.
		public int this[int index]
		{
			get
			{
				if (index < _lowerBound || index > _lowerBound + _length - 1)
				{
					throw new ArgumentOutOfRangeException("Index");
				}
				return _array[index - _lowerBound];
			}
			set
			{
				if (index < _lowerBound || index > _lowerBound + _length + 1)
				{
					throw new ArgumentOutOfRangeException("Index");
				}
				_array[index - _lowerBound] = value;
			}
		}
		#endregion
	
		#region Methods
		
		// Returns string representation of array.
		public override string ToString()
		{
			string outString = "[";
			for (int i = 0; i < _length; ++i)
			{
				outString = String.Format("{0} {1} ", outString, _array[i]);
			}
			outString += "]";
			return outString;
		}
		
		// Returns true if arrays have the same index intervals and equal elements.
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			
			if ( this.GetType() != obj.GetType())
			{
				return false;
			}
			
			OneDArrayNonZeroBound otherArray = (OneDArrayNonZeroBound)obj;
			if (this._lowerBound != otherArray._lowerBound || this._length != otherArray._length)
			{
				return false;
			}
			
			for (int i = LowerBound; i < UpperBound; i++)
			{
				if (this[i] != otherArray[i])
				{
					return false;
				}
			}
			return true;
		}
		
		// Returns the hash code value of the array.
		public override int GetHashCode()
		{
			return this.ToString().GetHashCode();
		}
		
		// Returns new array that represents sum of two specified arrays.
		// Index intervals mast be the same.
		public static OneDArrayNonZeroBound Add(OneDArrayNonZeroBound array1, OneDArrayNonZeroBound array2)
		{
			if (array1._lowerBound != array2._lowerBound || array1._length != array2._length)
			{
				throw new ArgumentException("Arrays have different index intervals", "array");
			}
			
			int lowerBound = array1._lowerBound;
			int length = array1._length;
			OneDArrayNonZeroBound resultArray = new OneDArrayNonZeroBound(length, lowerBound);
			
			for (int i = lowerBound; i < lowerBound + length; i++)
			{
				resultArray[i] = array1[i] + array2[i];
			}
			return resultArray;
		}
		
		
		// Returns new array that represents difference of two specified arrays.
		// Index intervals mast be the same.
		public static OneDArrayNonZeroBound Subtract(OneDArrayNonZeroBound array1, OneDArrayNonZeroBound array2)
		{
			if (array1._lowerBound != array2._lowerBound || array1._length != array2._length)
			{
				throw new ArgumentException("Arrays have different index intervals", "array");
			}
			
			int lowerBound = array1._lowerBound;
			int length = array1._length;
			OneDArrayNonZeroBound resultArray = new OneDArrayNonZeroBound(length, lowerBound);
			
			for (int i = lowerBound; i < lowerBound + length; i++)
			{
				resultArray[i] = array1[i] - array2[i];
			}
			return resultArray;
		}
		
		// Returns new array that is equal to array specified in arguments multiplied by scalar.
		public static OneDArrayNonZeroBound Multiply(OneDArrayNonZeroBound array, int scalar)
		{
			OneDArrayNonZeroBound resultArray = new OneDArrayNonZeroBound(array);
			resultArray.MultiplicationByScalar(scalar);
			return resultArray;
		}
		
		// Modifies current Array by adding another.
		public void Add(OneDArrayNonZeroBound array2)
		{
			if (this._lowerBound != array2._lowerBound || this._length != array2._length)
			{
				throw new ArgumentException("Arrays have different index intervals", "array");
			}
			
			for (int i = 0; i < _length; i++)
			{
				this._array[i] += array2._array[i];
			}
		}
		
		// Modifies current Array by subtracting another.
		public void Subtract(OneDArrayNonZeroBound array2)
		{
			if (this._lowerBound != array2._lowerBound || this._length != array2._length)
			{
				throw new ArgumentException("Arrays have different index intervals", "array");
			}
			
			for (int i = 0; i < _length; i++)
			{
				this._array[i] -= array2._array[i];
			}
		}
		
		// Multiplies all elements in array by a scalar.
		public void MultiplicationByScalar(int scalar)
		{
			for (int i = 0; i < _length; i++)
			{
				_array[i] *= scalar;
			}
		}
		
		// Returns int that represents comparison of two arrays.
		// Compares sum of elements absolute value.
		public int CompareTo(OneDArrayNonZeroBound otherArray)
		{
			return (this.sumOfAllElements()).CompareTo(otherArray.sumOfAllElements());
		}
		
		#endregion
		
		#region Operators
		
		// Array addition.
		public static OneDArrayNonZeroBound operator + (OneDArrayNonZeroBound array1, OneDArrayNonZeroBound array2)
		{
			return Add(array1, array2);
		}
		
		// Array subtraction.
		public static OneDArrayNonZeroBound operator - (OneDArrayNonZeroBound array1, OneDArrayNonZeroBound array2)
		{
			return Subtract(array1, array2);
		}
		
		// Scalar multiplication.
		public static OneDArrayNonZeroBound operator * (int scalar, OneDArrayNonZeroBound array)
		{
			OneDArrayNonZeroBound resultArray = new OneDArrayNonZeroBound(array);
			resultArray.MultiplicationByScalar(scalar);
			return resultArray;
		}
		
		// Scalar multiplication.
		public static OneDArrayNonZeroBound operator * (OneDArrayNonZeroBound array, int scalar)
		{
			return scalar * array;
		}
		
		// Operator <
		public static bool operator < (OneDArrayNonZeroBound array1, OneDArrayNonZeroBound array2)
		{
			return array1.CompareTo(array2) < 0;
		}
		
		// Operator >
		public static bool operator > (OneDArrayNonZeroBound array1, OneDArrayNonZeroBound array2)
		{
			return array1.CompareTo(array2) > 0;
		}
		
		// Operator <=
		public static bool operator <= (OneDArrayNonZeroBound array1, OneDArrayNonZeroBound array2)
		{
			return array1.CompareTo(array2) <= 0;
		}
		
		// Operator >=
		public static bool operator >= (OneDArrayNonZeroBound array1, OneDArrayNonZeroBound array2)
		{
			return array1.CompareTo(array2) >= 0;
		}
		
		// Operator ==
		public static bool operator == (OneDArrayNonZeroBound array1, OneDArrayNonZeroBound array2)
		{
			return array1.CompareTo(array2) == 0;
		}
		
		// Operator !=
		public static bool operator != (OneDArrayNonZeroBound array1, OneDArrayNonZeroBound array2)
		{
			return array1.CompareTo(array2) != 0;
		}
		
		#endregion
		
		#region PrivateMethods
		// Returns sum of absolute value of array elements.
		private int sumOfAllElements()
		{
			int sum = 0;
			for (int i = 0; i < _length; ++i)
			{
				sum += Math.Abs(_array[i]);
			}
			return sum;
		}
		#endregion
	}
	
	class Program
	{
		static void Main()
		{
			Console.WriteLine("\n\t Fun With One Dimensional Array With Non Zero Bound\n");
			
			OneDArrayNonZeroBound array1 = new OneDArrayNonZeroBound(10, 0);
			for (int i = array1.LowerBound; i < array1.UpperBound; i++)
			{
				array1[i] = i;
			}
			
			Console.WriteLine("First array:\n Length = {0}\n LowerBound = {1}\n {2}", array1.Length, array1.LowerBound, array1);
			
			Console.WriteLine("array1[{0}] = {1}", 0, array1[0]);
			Console.WriteLine("array1[{0}] = {1}", 9, array1[9]);
			try
			{
				Console.WriteLine("\nTry to access element with index 10 ");
				Console.WriteLine("\tarray1[{0}] = {1}", 10, array1[10]);
			}
			catch(ArgumentOutOfRangeException e)
			{
				Console.WriteLine("\tThere are no element with index 10 in array1.");
				Console.WriteLine(e.Message);
			}
			Console.WriteLine();
			
			OneDArrayNonZeroBound array2 = new OneDArrayNonZeroBound(10, 10);
			for (int i = array2.LowerBound; i < array2.UpperBound; i++)
			{
				array2[i] = i;
			}
			
			Console.WriteLine("Second array:\n Length = {0}\n LowerBound = {1}\n {2}", array2.Length, array2.LowerBound, array2);
			
			Console.WriteLine("array2[{0}] = {1}", 10, array2[10]);
			Console.WriteLine("array2[{0}] = {1}", 19, array2[19]);
			try
			{
				Console.WriteLine("\nTry to access element with index 0 ");
				Console.WriteLine("array2[{0}] = {1}", 0, array2[0]);
			}
			catch(ArgumentOutOfRangeException e)
			{
				Console.WriteLine("\tThere are no element with index 0 in array2.");
				Console.WriteLine(e.Message);
			}
			
			Console.WriteLine();
			
			OneDArrayNonZeroBound array3 = new OneDArrayNonZeroBound(10, 10);
			for (int i = array3.LowerBound; i < array3.UpperBound; i++)
			{
				array3[i] = 3;
			}
			Console.WriteLine("Third array:\n Length = {0}\n LowerBound = {1}\n {2}", array3.Length, array3.LowerBound, array3);
			
			Console.WriteLine();
			
			Console.WriteLine("operator: array2 + array3 = {0}", array2 + array3);
			Console.WriteLine("method:   Add(array2,array3) = {0}", OneDArrayNonZeroBound.Add(array2, array3));
			
			Console.WriteLine();
			
			Console.WriteLine("operator: array2 - array3 = {0}", array2 - array3);
			Console.WriteLine("method:   Subtract(array2, array3) = {0}", OneDArrayNonZeroBound.Subtract(array2, array3));
			
			Console.WriteLine();
			
			array3.Add(array2);
			Console.WriteLine("array3.Add(array2)\n array3 = {0}", array3);
			array3.Subtract(array2);
			Console.WriteLine("array3.Subtract(array2)\n array3  = {0}", array3);
			
			Console.WriteLine();
			
			try
			{
				Console.WriteLine("\nTry to add array1 and array2.");
				OneDArrayNonZeroBound array4 = array1 + array2;
			}
			catch(ArgumentException e)
			{
				Console.WriteLine(e.Message);
			}
			
			Console.WriteLine();
			
			Console.WriteLine("operator: array2 * 10 = {0}", array2 * 10);
			Console.WriteLine("operator: 10 * array2 = {0}", 10 * array2);
			Console.WriteLine("method:   Multiply(array2,10) = {0}", OneDArrayNonZeroBound.Multiply(array2, 10));
			array2.MultiplicationByScalar(10);
			Console.WriteLine("method MultiplicationByScalar(10):   \n\t array2 = {0}", array2);
			
			Console.WriteLine();
			
			Console.WriteLine("array1.Equals(array2) :   {0}", array1.Equals(array2));
			Console.WriteLine("array1 == array2 :   {0}", array1 == array2);
			Console.WriteLine("array1 != array2 :   {0}", array1 != array2);
			Console.WriteLine("array1 > array2 :   {0}", array1 > array2);
			Console.WriteLine("array1 < array2 :   {0}", array1 < array2);
			
		}
	}
}

