using System;

namespace Vector
{
	// This class implements 3 dimensional vector.
	class Vector : IComparable<Vector>
	{
		private double _x;
		private double _y;
		private double _z;
		
		#region Constructors
		
		// Default constructor.
		public Vector()
		{
		}
		// Constructor with three coordinate parameters (x, y, z).
		public Vector(double x, double y, double z)
		{
			_x = x;
			_y = y;
			_z = z;
		}
		#endregion
		
		#region Properties
		
		// Get X coordinate.
		public double X
		{
			get
			{
				return _x;
			}
			private set
			{
				_x = value;
			}
		}
		
		// Get Y coordinate.
		public double Y
		{
			get
			{
				return _y;
			}
			private set
			{
				_y = value;
			}
		}
		
		// Get Z coordinate.
		public double Z
		{
			get
			{
				return _z;
			}
			private set
			{
				_z = value;
			}
		}
		#endregion
		
		#region OverridenMethods
		
		// Returns string representation of vector.
		public override string ToString()
		{
			return String.Format("({0}, {1}, {2})", _x, _y, _z);
		}
		
		// Returns true if two vectors have equal coordinates.
		public override bool Equals(Object obj)
		{
			if(obj == null) 
			{
				return false;
			}
			
			if(this.GetType() != obj.GetType())
			{
				return false;
			}
			
			Vector other = (Vector)obj;
			return (this.X == other.X) && (this.Y == other.Y) && (this.Z == other.Z);
		}
		
		// Returns the hash code value for the vector.
		public override int GetHashCode()
		{
			return this.ToString().GetHashCode();
		}
		
		#endregion		
	
		#region Methods
		
		// Returns the length of the the vector.
		public double Length()
		{
			double length = Math.Sqrt( _x * _x + _y * _y + _z * _z);
			return length;
		}
		
		// Returns new vector that is sum of two other vectors specified in arguments.
		public static Vector Add(Vector vector1, Vector vector2)
		{
			return vector1 + vector2;
		}
		
		// Returns new vector that is difference of two other vectors specified in arguments.
		public static Vector Subtract(Vector vector1, Vector vector2)
		{
			return vector1 - vector2;
		}
		
		// Returns scalar that represents dot product of two vectors.
		public static double DotProduct(Vector vector1, Vector vector2)
		{
			return vector1 * vector2;
		}
		
		// Returns new vector that is vector product of two other vectors specified in arguments.
		public static Vector VectorProduct(Vector vector1, Vector vector2)
		{
			Vector resultVector = new Vector();
			resultVector.X = vector1.Y * vector2.Z - vector1.Z * vector2.Y;
			resultVector.Y = vector1.Z * vector2.X - vector1.X * vector2.Z;
			resultVector.Z = vector1.X * vector2.Y - vector1.Y * vector2.X;
			return resultVector;
		}
		
		// Returns new vector that is triple product of three other vectors specified in arguments.
		// Triple product is dot product of one of the vectors with the vector product of the other two.
		public static double TripleProduct(Vector vector1, Vector vector2, Vector vector3)
		{
			Vector vectProd_2_3 = VectorProduct(vector2, vector3);
			double tripleProduct = DotProduct(vector1, vectProd_2_3);
			return tripleProduct;
		}
		
		// Returns angle in radians between two vectors.
		public static double AngleBetweenVectors(Vector vector1, Vector vector2)
		{
			double angleCos = (vector1 * vector2) / (vector1.Length() * vector2.Length());
			double angle = Math.Acos(angleCos);
			return angle;
		}
		
		// Returns int that represents comparison this vector with another one.
		// Comparison is based on vector length.
		public int CompareTo(Vector otherVector)
		{
			double thisLength = this.Length();
			double otherLength = otherVector.Length();
			return thisLength.CompareTo(otherLength);
		}
		#endregion
				
		#region Operators
		
		// Vector addition.
		public static Vector operator + (Vector lhsVector, Vector rhsVector)
		{
			Vector resultVector = new Vector(lhsVector.X + rhsVector.X, lhsVector.Y + rhsVector.Y, lhsVector.Z + rhsVector.Z);
			return  resultVector;
		}
		
		// Vector subtraction.
		public static Vector operator - (Vector lhsVector, Vector rhsVector)
		{
			Vector resultVector = new Vector(lhsVector.X - rhsVector.X, lhsVector.Y - rhsVector.Y, lhsVector.Z - rhsVector.Z);
			return resultVector;
		}
		
		// Dot product.
		public static double operator * (Vector lhsVector, Vector rhsVector)
		{
			double result = lhsVector.X * rhsVector.X + lhsVector.Y * rhsVector.Y + lhsVector.Z * rhsVector.Z;
			return result;
		}
		
		// Operator <
		public static bool operator < (Vector vector1, Vector vector2)
		{
			return vector1.CompareTo(vector2) < 0;
		}
		
		// Operator >
		public static bool operator > (Vector vector1, Vector vector2)
		{
			return vector1.CompareTo(vector2) > 0;
		}
		
		// Operator <=
		public static bool operator <= (Vector vector1, Vector vector2)
		{
			return vector1.CompareTo(vector2) <= 0;
		}
		
		// Operator >=
		public static bool operator >= (Vector vector1, Vector vector2)
		{
			return vector1.CompareTo(vector2) >= 0;
		}
		
		// Operator ==
		// Operator is based on comparison of vector length, not on the equality of coordinates.
		public static bool operator == (Vector vector1, Vector vector2)
		{
			return vector1.CompareTo(vector2) == 0;
		}
		
		// Operator !=
		public static bool operator != (Vector vector1, Vector vector2)
		{
			return !(vector1 == vector2);
		}
		
		#endregion
	}
	
	class Program
	{
		static void Main()
		{
			Console.WriteLine("\n \t Fun with vectors!!! \n");
			
			Vector vector1 = new Vector(1, 0, 0);
			Vector vector2 = new Vector(0, 1, 0);
			Console.WriteLine(vector1);
			Console.WriteLine(vector2);
			
			Console.WriteLine("\n Addition");			
			Console.WriteLine("operator: {0} + {1} = {2}", vector1, vector2, vector1 + vector2);
			Console.WriteLine("method: {0} + {1} = {2}", vector1, vector2, Vector.Add(vector1, vector2));
			
			Console.WriteLine("\n Subtraction");			
			Console.WriteLine("operator: {0} - {1} = {2}", vector1, vector2, vector1 - vector2);
			Console.WriteLine("method: {0} - {1} = {2}", vector1, vector2, Vector.Subtract(vector1, vector2));
			
			Console.WriteLine("\n Dot product");
			Console.WriteLine("operator: {0} * {1} = {2}", vector1, vector2, vector1 * vector2);
			Console.WriteLine("method: {0} * {1} = {2}", vector1, vector2, Vector.DotProduct(vector1, vector2));
			
			Console.WriteLine(); 			
			Console.WriteLine("Vector {0} length = {1}", vector1, vector1.Length());
			
			Console.WriteLine();			
			Console.WriteLine("Vector product: [{0}, {1}] = {2}", vector1, vector2, Vector.VectorProduct(vector1, vector2));
			
			Console.WriteLine();			
			Vector vector3 = new Vector(0, 0, 1);
			Console.WriteLine("Triple product: {0} * [{1}, {2}] = {3}", vector1, vector2, vector3, Vector.TripleProduct(vector1, vector2, vector3));
			
			Console.WriteLine();
			Console.WriteLine("Angle between two vectors {0} and {1} is equal {2:f4} rad", vector1, vector2, Vector.AngleBetweenVectors(vector1, vector2));
			
			Console.WriteLine("\n Vector Comparison");
			Console.WriteLine(" {0} Equals {1} ? - {2}", vector1, vector2, vector1.Equals(vector2));
			Console.WriteLine(" {0} Equal to itself ? - {1}", vector1, vector1.Equals(vector1));
			Vector vector4 = new Vector(1, 0, 0);
			Console.WriteLine(" {0} Equals {1} ? - {2}", vector1, vector4, vector1.Equals(vector4));
			
			Console.WriteLine();
			Vector vector5 = new Vector(1, 1, 1);
			Vector vector6 = new Vector(2, 2, -2);
			Console.WriteLine("{0} < {1} = {2}", vector5, vector6, vector5 < vector6);
			Console.WriteLine("{0} > {1} = {2}", vector5, vector6, vector5 > vector6);
			Console.WriteLine("{0} <= {1} = {2}", vector5, vector6, vector5 <= vector6);
			Console.WriteLine("{0} >= {1} = {2}", vector5, vector6, vector5 >= vector6);
			Console.WriteLine("{0} == {1} = {2}", vector5, vector6, vector5 == vector6);
			Console.WriteLine("{0} != {1} = {2}", vector5, vector6, vector5 != vector6);	
		}
	}
}